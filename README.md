It turns out that, on current Android devices, it's actually possible to disable all wireless emergency alerts, including normally unblockable Presidental alerts.

# Notes (read before attempting)
This guide is an elaboration on [this Reddit post](https://www.reddit.com/r/ontario/comments/boxx11/how_to_disable_amber_alerts/).

This will not work on Samsung devices. The Reddit post provided an alternate solution for Samsung devices; I recommend using that.

You might see strange results if you have multiple user accounts on your phone (go to Settings → System → Multiple users to check). This won't be the case unless you've created another user on purpose.

I recommend doing this with a computer, although it is technically possible to do it with another phone. (In order to do that, both phones need to support USB-C and you need to choose "USB controlled by this device" on the device for which you want to disable emergency alerts. Then you can use the chrome web app mentioned later to send adb commands.)

# Step 1: Enable USB debugging
Open the Settings app and tap on About phone.

![screenshot](imgs/01.png)

Scroll down to Build number. Tap on Build number rapidly. It will ask you to enter your PIN, and then a message saying "You are now a developer!" will appear.

![screenshot](imgs/02.png)

![screenshot](imgs/03.png)

Go back to the main screen of the Settings app and go to System → Developer Options.

![screenshot](imgs/04.png)

Scroll down to USB debugging and enable it.

![screenshot](imgs/05.png)

# Step 2: Access ADB
You can install ADB onto your computer and use it in the terminal by following [this guide](https://www.xda-developers.com/install-adb-windows-macos-linux/), or you can use [this webpage](https://webadb.github.io/) in Google Chrome. I will show screenshots for both. Note that in the web app, commands should be entered in the box next to the Send button.

Connect your device to your computer over USB. If using adb in the terminal, run `adb devices` and make sure your device appears. If using the web app, follow the prompts on screen to connect your device.

# Step 3: Make sure this can work for your device
Run the command `adb shell "pm list packages -u | grep cellbroadcastreceiver"`. You should see a result that looks like
```
package:com.android.cellbroadcastreceiver
package:com.android.cellbroadcastreceiver.module
```
If you do not see this, then **this will not work on your device.**

![screenshot](imgs/06.png)

In the web app, that's `shell:pm list packages -u | grep cellbroadcastreceiver` (click Send).

![screenshot](imgs/06a.png)

# Step 4: "Uninstall" the cell broadcast receiver app
Run the command `adb shell "pm uninstall -k --user 0 <package_name>` for each of the package names you got as a result of the previous command. So for my phone, it would be
```
adb shell "pm uninstall -k --user 0 com.android.cellbroadcastreceiver"
adb shell "pm uninstall -k --user 0 com.android.cellbroadcastreceiver.module"
```
You should see a `Success` message.

![screenshot](imgs/07.png)

In the web app, that's `shell:pm uninstall -k --user 0 <package_name>` (click Send) for each of the package names you got as a result of the previous command. For my device, I did the following two commands:

![screenshot](imgs/07a.png)

![screenshot](imgs/07b.png)

# Step 5: Check to make sure that the app was uninstalled
Now, if you go to Settings → Apps → See all apps (show system) → Search, and search for "emergency," there should be no results.

![screenshot](imgs/08.png)

![screenshot](imgs/09.png)

![screenshot](imgs/10.png)

![screenshot](imgs/11.png)

# If you ever need to re-enable emergency alerts
If you need to re-enable emergency alerts, for each package name that you got from step 3 (you can run it again if you don't remember), run the command `adb shell "cmd package install-existing <package_name>"`(or in the web app, `shell:cmd package install-existing <package_name>`). So for my phone, it would be
```
adb shell "cmd package install-existing com.android.cellbroadcastreceiver"
adb shell "cmd package install-existing com.android.cellbroadcastreceiver.module"
```
![screenshot](imgs/12.png)

In the web app, I did the following two commands:

![screenshot](imgs/12a.png)

![screenshot](imgs/12b.png)

